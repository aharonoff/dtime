var colors = {
  white: [255, 255, 255],
  black: [0, 0, 0]
}

var deeptime = [
  [9, "The western black rhinoceros is declared extinct."],
  [12, "The baiji, the Yangtze river dolphin, becomes functionally extinct, according to the IUCN Red List."],
  [68, "The Caribbean monk seal goes extinct."],
  [84, "The thylacine goes extinct in a Tasmanian zoo, the last member of the family Thylacinidae."],
  [106, "Martha, the last known passenger pigeon, dies."],
  [115, "Wolves become extinct in Japan."],
  [137, "The quagga, a subspecies of zebra, goes extinct."],
  [252, "The Steller's sea cow goes extinct."],
  [332, "The dodo goes extinct."],
  [393, "The last recorded wild aurochs die out."],
  [600, "The moa and its predator, Haast's eagle, die out in New Zealand."],
  [4500, "The last members of a dwarf race of woolly mammoths vanish from Wrangel Island near Alaska."],
  [6000, "Small populations of American mastodon die off in places like Utah and Michigan."],
  [8000, "The Giant Lemur died out."],
  [10000, "The Holocene epoch starts after the Late Glacial Maximum. The last mainland species of woolly mammoth die out."],
  [11000, "Short-faced bears vanish from North America, with the last giant ground sloths dying out. All Equidae become extinct in North America."],
  [15000, "The last woolly rhinoceros (Coelodonta antiquitatis) are believed to have gone extinct."],
  [30000, "Extinction of Neanderthals, first domestic dogs."],
  [40000, "The last of the giant monitor lizards (Varanus priscus) die out."],
  [250000, "Anatomically modern humans appear in Africa."],
  [300000, "Gigantopithecus, a giant relative of the orangutan from Asia dies out."],
  [350000, "Evolution of Neanderthals."],
  [400000, "First polar bears."],
  [600000, "Evolution of Homo heidelbergensis"],
  [800000, "Short-faced bears (Arctodus simus) become abundant in North America."],
  [1000000, "First coyotes."],
  [1200000, "Evolution of Homo antecessor. The last members of Paranthropus die out."],
  [1700000, "Extinction of australopithecines."],
  [2000000, "First members of the genus Homo, Homo Habilis, appear in the fossil record. Diversification of conifers in high latitudes. The eventual ancestor of cattle, aurochs, evolves in India."],
  [2500000, "The earliest species of Smilodon evolve."],
  [2700000, "Evolution of Paranthropus."],
  [3000000, "Earliest swordfish."],
  [3600000, "Blue whales grow to their modern sizes."],
  [4000000, "Evolution of Australopithecus, Stupendemys appears in the fossil record as the largest freshwater turtle, first modern elephants, giraffes, zebras, lions, rhinoceros and gazelles appear in the fossil record."],
  [4500000, "Marine iguanas diverge from land iguanas."],
  [4800000, "Mammoths appear in the fossil record."],
  [5000000, "First tree sloths and hippopotami, diversification of grazing herbivores like zebras and elephants, large carnivorous mammals like lions and the genus Canis, burrowing rodents, kangaroos, birds, and small carnivores, vultures increase in size, decrease in the number of perissodactyl mammals. Extinction of nimravid carnivores. First leopard seals."],
  [6000000, "Australopithecines diversify (Orrorin, Ardipithecus)."],
  [6500000, "First hominins (Sahelanthropus)."],
  [9000000, "First platypuses."],
  [10000000, "Grasslands and savannas are established, diversity in insects, especially ants and termites, horses increase in body size and develop high-crowned teeth, major diversification in grassland mammals and snakes."],
  [15000000, "Genus Mammut appears in the fossil record, first bovids and kangaroos, diversity in Australian megafauna."],
  [17000000, "First birds of the genus Corvus (crows)."],
  [20000000, "First giraffes, hyenas, and giant anteaters, increase in bird diversity."],
  [23000000, "Earliest ostriches, trees representative of most major groups of oaks have appeared by now."],
  [24000000, "First pinnipeds."],
  [25000000, "Pelagornis sandersi appears in the fossil record, the largest flying bird that ever lived. First deer."],
  [28000000, "Paraceratherium appears in the fossil record, the largest terrestrial mammal that ever lived. First pelicans."],
  [30000000, "First balanids and eucalypts, extinction of embrithopod and brontothere mammals, earliest pigs and cats."],
  [33000000, "Evolution of the thylacinid marsupials (Badjcinus)."],
  [35000000, "Grasses diversify from among the monocot angiosperms; grasslands begin to expand. Slight increase in diversity of cold-tolerant ostracods and foraminifers, along with major extinctions of gastropods, reptiles, amphibians, and multituberculate mammals. Many modern mammal groups begin to appear: first glyptodonts, ground sloths, canids, peccaries, and the first eagles and hawks. Diversity in toothed and baleen whales."],
  [37000000, "First nimravid ('false saber-toothed cats') carnivores — these species are unrelated to modern-type felines. First alligators."],
  [38000000, "Earliest bears."],
  [40000000, "Modern-type butterflies and moths appear. Extinction of Gastornis. Basilosaurus, one of the first of the giant whales, appeared in the fossil record."],
  [50000000, "Peak diversity of dinoflagellates and nannofossils, increase in diversity of anomalodesmatan and heteroconch bivalves, brontotheres, tapirs, rhinoceroses, and camels appear in the fossil record, diversification of primates."],
  [52000000, "First bats appear (Onychonycteris)."],
  [55000000, "Modern bird groups diversify (first song birds, parrots, loons, swifts, woodpeckers), first whale (Himalayacetus), earliest lagomorphs, armadillos, appearance of sirenian, proboscidean, perissodactyl and artiodactyl mammals in the fossil record. Angiosperms diversify. The ancestor (according to theory) of the species in the genus Carcharodon, the early mako shark Isurus hastalis, is alive."],
  [56000000, "Gastornis, a large flightless bird, appears in the fossil record."],
  [59000000, "Earliest sailfish appear."],
  [60000000, "Diversification of large, flightless birds. The first semelid bivalves, edentate, carnivoran and lipotyphlan mammals, and owls."],
  [62000000, "Evolution of the first penguins."],
  [63000000, "Evolution of the creodonts, an important group of meat-eating (carnivorous) mammals."],
  [66000000, "The Cretaceous–Paleogene extinction event eradicates about half of all animal species, including mosasaurs, pterosaurs, plesiosaurs, ammonites, belemnites, rudist and inoceramid bivalves, most planktic foraminifers, and all of the dinosaurs excluding the birds. Rapid dominance of conifers and ginkgos in high latitudes, along with mammals becoming the dominant species. First psammobiid bivalves. Earliest rodents. Rapid diversification in ants."],
  [68000000, "Tyrannosaurus, the largest terrestrial predator of what is now western North America appears in the fossil record. First species of Triceratops."],
  [70000000, "Multituberculate mammals increase in diversity. First yoldiid bivalves."],
  [80000000, "First ants."],
  [90000000, "Extinction of ichthyosaurs. Earliest snakes and nuculanid bivalves. Large diversification in angiosperms: magnoliids, rosids, hamamelidids, monocots, and ginger. Earliest examples of ticks."],
  [95000000, "First crocodilians evolve."],
  [100000000, "Earliest bees."],
  [106000000, "Spinosaurus, the largest theropod dinosaur, appears in the fossil record."],
  [110000000, "First hesperornithes, toothed diving birds. Earliest limopsid, verticordiid, and thyasirid bivalves."],
  [112000000, "Xiphactinus, a large predatory fish, appears in the fossil record."],
  [115000000, "First monotreme mammals."],
  [120000000, "Oldest fossils of heterokonts, including both marine diatoms and silicoflagellates."],
  [130000000, "The rise of the angiosperms: Some of these flowering plants bear structures that attract insects and other animals to spread pollen; other angiosperms were pollinated by wind or water. This innovation causes a major burst of animal evolution through coevolution. First freshwater pelomedusid turtles. Earliest krill."],
  [140000000, "Orb-weaver spiders appear."],
  [153000000, "First pine trees."],
  [155000000, "First blood-sucking insects (ceratopogonids), rudist bivalves, and cheilostome bryozoans. Archaeopteryx, a possible ancestor to the birds, appears in the fossil record, along with triconodontid and symmetrodont mammals. Diversity in stegosaurian and theropod dinosaurs."],
  [160000000, "Multituberculate mammals (genus Rugosodon) appear in eastern China."],
  [161000000, "Ceratopsian dinosaurs appear in the fossil record (Yinlong) and the oldest known Eutherian Mammal appear in the fossil record: Juramaia."],
  [163000000, "Pterodactyloid pterosaurs first appear."],
  [165000000, "First rays and glycymeridid bivalves. First vampire squids."],
  [170000000, "Earliest salamanders, newts, cryptoclidids, elasmosaurid plesiosaurs, and cladotherian mammals. Sauropod dinosaurs diversify."],
  [176000000, "First members of the Stegosauria group of dinosaurs."],
  [190000000, "Pliosauroids appear in the fossil record. First lepidopteran insects (Archaeolepis), hermit crabs, modern starfish, irregular echinoids, corbulid bivalves, and tubulipore bryozoans. Extensive development of sponge reefs."],
  [195000000, "First pterosaurs with specialized feeding (Dorygnathus). First sauropod dinosaurs. Diversification in small, ornithischian dinosaurs: heterodontosaurids, fabrosaurids, and scelidosaurids."],
  [200000000, "The first accepted evidence for viruses that infect eukaryotic cells (at least, the group Geminiviridae) existed. Viruses are still poorly understood and may have arisen before 'life' itself, or may be a more recent phenomenon. Major extinctions in terrestrial vertebrates and large amphibians. Earliest examples of armoured dinosaurs "],
  [205000000, "The Massive extinction of Triassic/Jurassic, that wiped out most of the group of pseudosuchians and gave the opportunity of dinosaurs including the Apatosaurus, Tyrannosaurus, Perrottasaurus, and Stegosaurus to enter their golden age."],
  [220000000, "Seed-producing Gymnosperm forests dominate the land. First flies and turtles (Odontochelys). First coelophysoid dinosaurs."],
  [225000000, "Earliest dinosaurs (prosauropods), first cardiid bivalves, diversity in cycads, bennettitaleans, and conifers. First teleost fishes. First mammals (Adelobasileus)."],
  [240000000, "Increase in diversity of gomphodont cynodonts and rhynchosaurs."],
  [245000000, "Earliest ichthyosaurs."],
  [248000000, "Sturgeon and paddlefish (Acipenseridae) first appear."],
  [250000000, "The Mesozoic Marine Revolution begins: increasingly well adapted and diverse predators pressurize sessile marine groups; the 'balance of power' in the oceans shifts dramatically as some groups of prey adapt more rapidly and effectively than others. Triadobatrachus massinoti is the earliest known frog."],
  [251400000, "The Permian–Triassic extinction event eliminates over 90-95% of marine species. Terrestrial organisms were not as seriously affected as the marine biota. This 'clearing of the slate' may have led to an ensuing diversification, but life on land took 30 million years to completely recover."],
  [270000000, "Gorgonopsians appear in the fossil record."],
  [275000000, "Therapsid synapsids separate from pelycosaur synapsids."],
  [280000000, "Earliest beetles, seed plants and conifers diversify while lepidodendrids and sphenopsids decrease. Terrestrial temnospondyl amphibians and pelycosaurs (e.g. Dimetrodon) diversify in species."],
  [296000000, "Earliest known octopus (Pohlsepia)."],
  [305000000, "Earliest diapsid reptiles (e.g. Petrolacosaurus)."],
  [320000000, "Synapsids (precursors to mammals) separate from sauropsids (reptiles) in late Carboniferous."],
  [330000000, "First amniote vertebrates (Paleothyris)."],
  [340000000, "Diversification of amphibians."],
  [350000000, "First large sharks, ratfishes, and hagfish."],
  [360000000, "First crabs and ferns. Land flora dominated by seed ferns. The Xinhang forest grows around this time."],
  [363000000, "By the start of the Carboniferous Period, the Earth begins to resemble its present state. Insects roamed the land and would soon take to the skies; sharks swam the oceans as top predators, and vegetation covered the land, with seed-bearing plants and forests soon to flourish. Four-limbed tetrapods gradually gain adaptations which will help them occupy a terrestrial life-habit."],
  [365000000, "Acanthostega is one of the earliest vertebrates capable of walking."],
  [395000000, "First lichens, stoneworts. Earliest harvestmen, mites, hexapods (springtails) and ammonoids. The first known tetrapod tracks on land."],
  [410000000, "First signs of teeth in fish. Earliest Nautilida, lycophytes, and trimerophytes."],
  [420000000, "Earliest ray-finned fishes, trigonotarbid arachnids, and land scorpions."],
  [440000000, "First agnathan fishes: Heterostraci, Galeaspida, and Pituriaspida."],
  [450000000, "First complete conodonts and echinoids appear."],
  [485000000, "First vertebrates with true bones (jawless fishes)."],
  [500000000, "Jellyfish have existed since at least this time."],
  [505000000, "Fossilization of the Burgess Shale."],
  [510000000, "First cephalopods (nautiloids) and chitons."],
  [511000000, "Earliest crustaceans."],
  [525000000, "Earliest graptolites."],
  [530000000, "The first known footprints on land."],
  [535000000, "Major diversification of living things in the oceans: chordates, arthropods (e.g. trilobites, crustaceans), echinoderms, molluscs, brachiopods, foraminifers and radiolarians, etc."],
  [550000000, "First fossil evidence for Ctenophora (comb jellies), Porifera (sponges), Anthozoa (corals and sea anemones). Appearance of Ikaria wariootia (an early Bilaterian)."],
  [580000000, "The Ediacara biota represent the first large, complex aquatic multicellular organisms — although their affinities remain a subject of debate. Most modern phyla of animals begin to appear in the fossil record during the Cambrian explosion."],
  [600000000, "The accumulation of atmospheric oxygen allows the formation of an ozone layer. Prior to this, land-based life would probably have required other chemicals to attenuate ultraviolet radiation enough to permit colonisation of the land."],
  [750000000, "First protozoa (ex: Melanocyrillium); beginning of animal evolution."],
  [850000000, "A global glaciation may have occurred. Opinion is divided on whether it increased or decreased biodiversity or the rate of evolution. It is believed that this was due to evolution of the first land plants, which increased the amount of oxygen and lowered the amount of carbon dioxide in the atmosphere."],
  [1000000000, "The first non-marine eukaryotes move onto land. They were photosynthetic and multicellular, indicating that plants evolved much earlier than originally thought."],
  [1200000000, "Meiosis and sexual reproduction are present in single-celled eukaryotes, and possibly in the common ancestor of all eukaryotes.[44] Sex may even have arisen earlier in the RNA world. Sexual reproduction first appears in the fossil records; it may have increased the rate of evolution."],
  [1300000000, "Earliest land fungi."],
  [1400000000, "Great increase in stromatolite diversity."],
  [1850000000, "Eukaryotic cells appear. Eukaryotes contain membrane-bound organelles with diverse functions, probably derived from prokaryotes engulfing each other via phagocytosis. Bacterial viruses (bacteriophage) emerge before, or soon after, the divergence of the prokaryotic and eukaryotic lineages. The appearance of red beds show that an oxidising atmosphere had been produced. Incentives now favoured the spread of eukaryotic life."],
  [2500000000, "Great Oxidation Event led by cyanobacteria's oxygenic photosynthesis. Commencement of plate tectonics with old marine crust dense enough to subduct."],
  [2800000000, "Oldest evidence for microbial life on land in the form of organic matter-rich paleosols, ephemeral ponds and alluvial sequences, some of them bearing microfossils."],
  [3000000000, "Photosynthesizing cyanobacteria evolved; they used water as a reducing agent, thereby producing oxygen as a waste product. The oxygen initially oxidizes dissolved iron in the oceans, creating iron ore. The oxygen concentration in the atmosphere slowly rose, acting as a poison for many bacteria and eventually triggering the Great Oxygenation Event."],
  [3200000000, "Diversification and expansion of acritarchs."],
  [3500000000, "Lifetime of the last universal common ancestor (LUCA); the split between bacteria and archaea occurs. Bacteria develop primitive forms of photosynthesis which at first did not produce oxygen. These organisms generated Adenosine triphosphate (ATP) by exploiting a proton gradient, a mechanism still used in virtually all organisms."],
  [3800000000, "Formation of a greenstone belt of the Isua complex of the western Greenland region, whose rocks show an isotope frequency suggestive of the presence of life. The earliest evidences for life on Earth are 3.8 billion-year-old biogenic hematite in a banded iron formation of the Nuvvuagittuq Greenstone Belt in Canada, graphite in 3.7 billion-year-old metasedimentary rocks discovered in western Greenland and microbial mat fossils found in 3.48 billion-year-old sandstone discovered in Western Australia."],
  [3900000000, "Cells resembling prokaryotes appear. These first organisms are chemoautotrophs: they use carbon dioxide as a carbon source and oxidize inorganic materials to extract energy. Later, prokaryotes evolve glycolysis, a set of chemical reactions that free the energy of organic molecules such as glucose and store it in the chemical bonds of ATP. Glycolysis (and ATP) continue to be used in almost all organisms, unchanged, to this day."],
  [4000000000, "Formation of a greenstone belt of the Acasta Gneiss of the Slave craton in Northwest Territories, Canada, the oldest rock belt in the world."],
  [4100000000, "Late Heavy Bombardment (LHB): extended barrage of impact events upon the inner planets by meteoroids. Thermal flux from widespread hydrothermal activity during the LHB may have been conducive to abiogenesis and life's early diversification. 'Remains of biotic life' were found in 4.1 billion-year-old rocks in Western Australia. This is when life most likely arose."],
  [4280000000, "Earliest possible appearance of life on Earth."],
  [4374000000, "The age of the oldest discovered zircon crystals."],
  [4400000000, "First appearance of liquid water on Earth."],
  [4500000000, "According to the giant impact hypothesis, the Moon originated when the planet Earth and the hypothesized planet Theia collided, sending a very large number of moonlets into orbit around the young Earth which eventually coalesced to form the Moon. The gravitational pull of the new Moon stabilised the Earth's fluctuating axis of rotation and set up the conditions in which abiogenesis could occur."],
  [4600000000, "The planet Earth forms from the accretion disc revolving around the young Sun, with organic compounds (complex organic molecules) necessary for life having perhaps formed in the protoplanetary disk of cosmic dust grains surrounding it before the formation of the Earth itself."]
]

var texts = {
  font: null,
  desc: 'The human brain is good at so much,\nyet so bad at big numbers.\nNowhere we do fail harder than in deep time.\nWe do not really understand it at all.\n\nStart scrolling to figure it out...'
}

var pos = 0

function preload() {
  texts.font = loadFont('ttf/linlib.ttf')
}

function setup() {
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  background(colors.black)
  textSize(16)
  rectMode(CENTER)
  textFont(texts.font)

  // description
  textAlign(LEFT, BASELINE)
  fill(colors.white)
  noStroke()
  text(texts.desc, 16, 32 - 10 * pos)

  // years
  fill(colors.white)
  noStroke()
  triangle(windowWidth - 20, windowHeight * 0.5 - 4, windowWidth - 32, windowHeight * 0.5 - 12, windowWidth - 32, windowHeight * 0.5 + 4)
  textAlign(RIGHT, BASELINE)
  fill(colors.white)
  noStroke()
  text(Math.floor(pos).toLocaleString("en-US") + ' ya', windowWidth - 40, windowHeight * 0.5)

  // events
  rectMode(CORNERS)
  textAlign(RIGHT, BASELINE)
  fill(colors.white)
  noStroke()
  for (var i = 0; i < deeptime.length; i++) {
    text(deeptime[i][1], 100, windowHeight * 0.5 + 10 * (deeptime[i][0] - pos), windowWidth - 256)
  }

  // measures
  for (var i = 0; i < 200; i++) {
    fill(colors.white)
    stroke(colors.white)
    strokeWeight(2)
    if (i % 10 === 0) {
      line(windowWidth, windowHeight * 0.5 - 4 + (100 - i) * 10 - (pos % 100), windowWidth - 16, windowHeight * 0.5 - 4 + (100 - i) * 10 - (pos % 100))
    } else if (i % 5 === 0) {
      line(windowWidth, windowHeight * 0.5 - 4 + (100 - i) * 10 - (pos % 100), windowWidth - 8, windowHeight * 0.5 - 4 + (100 - i) * 10 - (pos % 100))
    } else {
      line(windowWidth, windowHeight * 0.5 - 4 + (100 - i) * 10 - (pos % 100), windowWidth - 4, windowHeight * 0.5 - 4 + (100 - i) * 10 - (pos % 100))
    }
  }

  if (pos > 4600000000) {
    pos = 4600000000
  }
  if (pos < 0) {
    pos = 0
  }
}

function mouseWheel(event) {
  if (pos > 4600000000) {
    pos = 4600000000
  }
  if (pos < 0) {
    pos = 0
  }
  pos += event.delta * 0.5
}

function mouseClicked() {
  for (var i = 0; i < deeptime.length; i++) {
    if (deeptime[i][0] > pos) {
      pos = deeptime[i][0]
      break
    }
  }
}

function windowResized() {
  createCanvas(windowWidth, windowHeight)
}
